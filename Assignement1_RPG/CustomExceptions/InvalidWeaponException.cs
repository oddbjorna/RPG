﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException()
        {
        }

        public override string Message => "Either your level is too low or the weapon is the wrong type!";
    }
}

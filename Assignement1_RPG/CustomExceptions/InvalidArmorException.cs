﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException()
        {
        }
        public override string Message => "Either your level is too low, the armor is the wrong type or you are trying to equip the armor in the 'Hand' slot!";
    }
}

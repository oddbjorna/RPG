﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Rogue : BaseClass
    {
        /// <summary>
        /// Rogue class inheriting from BaseClass.
        /// Contructor sets the stats for a newly created character and adds the allowed weapon types and armor types.
        /// LevelUp and ReturnDamage methods are overridden from the BaseClass methods to use the proper stats.
        /// </summary>
        public Rogue()
        {
            ClassName = "Rogue";
            SetPrimaryStats(8, 2, 6, 1);
            SetSecondaryStats();
            List<BaseWeapon.WType> allowedWeapons = new();
            List<BaseArmor.AType> allowedArmor = new();
            allowedWeapons.Add(BaseWeapon.WType.Daggers);
            allowedWeapons.Add(BaseWeapon.WType.Sword);
            allowedArmor.Add(BaseArmor.AType.Leather);
            allowedArmor.Add(BaseArmor.AType.Mail);
            AllowedArmorType = allowedArmor;
            AllowedWeaponType = allowedWeapons;
        }

        public override void LevelUp(int increase = 1)
        {
            if (increase < 1)
            {
                throw new ArgumentException();
            }
            GainLevel(1, 3, 1, 4, 1, increase);
        }
        public override void ReturnDamage()
        {
            CalcDamage(TotalDex);
        }
    }
}

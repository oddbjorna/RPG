﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Ranger : BaseClass
    {
        /// <summary>
        /// Ranger class inheriting from BaseClass.
        /// Contructor sets the stats for a newly created character and adds the allowed weapon types and armor types.
        /// LevelUp and ReturnDamage methods are overridden from the BaseClass methods to use the proper stats.
        /// </summary>
        public Ranger()
        {
            ClassName = "Ranger";
            SetPrimaryStats(8, 1, 7, 1);
            SetSecondaryStats();
            List<BaseWeapon.WType> allowedWeapons = new();
            List<BaseArmor.AType> allowedArmor = new();
            allowedWeapons.Add(BaseWeapon.WType.Bow);
            allowedArmor.Add(BaseArmor.AType.Leather);
            allowedArmor.Add(BaseArmor.AType.Mail);
            AllowedArmorType = allowedArmor;
            AllowedWeaponType = allowedWeapons;
        }

        public override void LevelUp(int increase = 1)
        {
            if (increase < 1)
            {
                throw new ArgumentException();
            }
            GainLevel(1, 2, 1, 5, 1, increase);
        }
        public override void ReturnDamage()
        {
            CalcDamage(TotalDex);
        }
    }
}

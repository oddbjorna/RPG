﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public abstract class BaseClass : Stats
    {
        private int level = 1;
        public int Level { get => level; set => level = value; }
        public string ClassName { get; set; }
        public double TotalDamage { get; set; }
        public double TotalDPS { get; set; } = 1;

        public List<BaseWeapon.WType> AllowedWeaponType { get; set; }
        public List<BaseArmor.AType> AllowedArmorType { get; set; }



        // Equipment
        public Dictionary<Item.Slot, Item> equipment = new();

        // Setting stats
        public void SetPrimaryStats(int vit, int str, int dex, int intl)
        {
            Vitality = vit;
            Strength = str;
            Dexterity = dex;
            Intelligence = intl;

            TotalVit += vit;
            TotalStr += str;
            TotalDex += dex;
            TotalInt += intl;

            PrimaryStats["Vitality"] = TotalVit;
            PrimaryStats["Strength"] = TotalStr;
            PrimaryStats["Dexterity"] = TotalDex;
            PrimaryStats["Intelligence"] = TotalInt;
        }
        public void SetSecondaryStats()
        {
            Health = TotalVit * 10;
            ArmorRating = TotalStr + TotalDex;
            ElementalResistance = TotalInt;

            SecondaryStats["Health"] = Health;
            SecondaryStats["ArmorRating"] = ArmorRating;
            SecondaryStats["ElementalResistance"] = ElementalResistance;
        }

        /// <summary>
        /// ReturnDamage is a shell method for encapsulating the CalcDamage method.
        /// It is done so that the method can be overriden by the correct primary stat for the correct class.
        /// CalcDamage checks if there is a weapon equipped. If so it calculates the TotalDPS by multiplying the weapon DPS times 1% increase of each point of primary stat.
        /// </summary>
        public abstract void ReturnDamage();
        public void CalcDamage(double attr)
        {
            try
            {
                TotalDPS = equipment[Item.Slot.Hand].DPS * (1 + (attr / 100));
                Console.WriteLine($"*    Current total DPS: {TotalDPS}\n");
            }
            catch
            {
                TotalDPS = 1;
                Console.WriteLine($"*    Current total DPS: {TotalDPS}\n");
            }
        }

        /// <summary>
        /// The LevelUp method is a shell method for encapsulating the GainLevel method.
        /// It is done so that the method could be called and the correct stats would be calculating depending on what class is calling the method.
        /// The GainLevel method increases all the stats by their correct values. There is an optional parameter 'increase' that is used for testing.
        /// Passing this parameter will increase the characters level and stats by x amount.
        /// After the character has gained new stats, SetSecondaryStats is called to update the secondary stats with their correct values.
        /// </summary>
        /// <param name="increase"></param>
        public abstract void LevelUp(int increase = 1);

        public void GainLevel(int lvl, int vit, int str, int dex, int intl, int increase = 1)
        {
            Level += lvl * increase;
            Vitality += vit * increase;
            Strength += str * increase;
            Dexterity += dex * increase;
            Intelligence += intl * increase;

            TotalVit += vit * increase;
            TotalStr += str * increase;
            TotalDex += dex * increase;
            TotalInt += intl * increase;

            PrimaryStats["Vitality"] = TotalVit;
            PrimaryStats["Strength"] = TotalStr;
            PrimaryStats["Dexterity"] = TotalDex;
            PrimaryStats["Intelligence"] = TotalInt;

            SetSecondaryStats();
        }

        /// <summary>
        /// The EquipWeapon method takes a Weapon parameter and checks if it can equip it.
        /// If the weapon is not in the characters AllowedWeaponType, InvalidWeaponException will be thrown.
        /// If the level of the weapon is higher than the characters current weapon, InvalidWeaponException will be thrown.
        /// If the Character already has a weapon equiped, the equiped weapon will be unequiped, and the stats on the weapon will be subtracted from the characters Total stats. Then the new weapon will be equiped.
        /// If the character does not have anything equiped the new weapon will be equiped. 
        /// Afterwards all the stats of the weapon will be added to the characters Total stats, and secondary stats will be calculated.
        /// </summary>
        /// <param name="wep"></param>
        public string EquipWeapon(Weapon wep)
        {
            if (AllowedWeaponType.Contains(wep.WeaponType))
            {
                if (Level >= wep.LevelToEquip)
                {
                    if (equipment.ContainsKey(Item.Slot.Hand))
                    {
                        TotalVit -= equipment[Item.Slot.Hand].Vitality;
                        TotalStr -= equipment[Item.Slot.Hand].Strength;
                        TotalDex -= equipment[Item.Slot.Hand].Dexterity;
                        TotalInt -= equipment[Item.Slot.Hand].Intelligence;
                        equipment.Remove(Item.Slot.Hand);
                        equipment.Add(Item.Slot.Hand, wep);
                    }
                    else
                    {
                        equipment.Add(Item.Slot.Hand, wep);
                    }

                    TotalVit += equipment[Item.Slot.Hand].Vitality;
                    TotalStr += equipment[Item.Slot.Hand].Strength;
                    TotalDex += equipment[Item.Slot.Hand].Dexterity;
                    TotalInt += equipment[Item.Slot.Hand].Intelligence;

                    SetSecondaryStats();
                    Console.WriteLine($"*    You try to equip {wep.Name}");
                    Console.WriteLine($"*    You equipped {wep.Name}!\n");

                    return "New Weapon Equipped!";
                }
                else
                {
                    throw new InvalidWeaponException();
                }

            }
            else
            {

                throw new InvalidWeaponException();

            }
        }

        /// <summary>
        /// EquipArmor is nearly identical to EquipWeapon, but it has an extra parameter: slot.
        /// It checks if the provided slot is a 'Hand' slot, and if it is it will throw an InvalidArmorException.
        /// If the slot is not a 'Hand' slot it will add the armor, and its stats, to the corresponding slot, removing existing armor in the process.
        /// Afterwards it calculates the secondary stats.
        /// </summary>
        /// <param name="armor"></param>
        /// <param name="slot"></param>
        public string EquipArmor(Armor armor, Item.Slot slot)
        {
            if (slot != Item.Slot.Hand)
            {
                if (AllowedArmorType.Contains(armor.ArmorType))
                {
                    if (Level >= armor.LevelToEquip)
                    {
                        if (equipment.ContainsKey(slot))
                        {
                            TotalVit -= equipment[slot].Vitality;
                            TotalStr -= equipment[slot].Strength;
                            TotalDex -= equipment[slot].Dexterity;
                            TotalInt -= equipment[slot].Intelligence;
                            equipment.Remove(slot);
                            equipment.Add(slot, armor);
                        }
                        else
                        {
                            equipment.Add(slot, armor);
                        }

                        TotalVit += equipment[slot].Vitality;
                        TotalStr += equipment[slot].Strength;
                        TotalDex += equipment[slot].Dexterity;
                        TotalInt += equipment[slot].Intelligence;

                        SetSecondaryStats();
                        Console.WriteLine($"*    You try to equip {armor.Name}");
                        Console.WriteLine($"*    You equipped {armor.Name}!\n");

                        return "New Armor Equipped!";
                    }
                    else
                    {
                        throw new InvalidArmorException();

                    }

                }
                else
                {
                    throw new InvalidArmorException();
                }
            }
            else
            {
                throw new InvalidArmorException();
            }

        }
    }
}

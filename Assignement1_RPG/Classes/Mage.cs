﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    /// <summary>
    /// Mage class inheriting from BaseClass.
    /// Contructor sets the stats for a newly created character and adds the allowed weapon types and armor types.
    /// LevelUp and ReturnDamage methods are overridden from the BaseClass methods to use the proper stats.
    /// </summary>
    public class Mage : BaseClass
    {
        public Mage()
        {
            ClassName = "Mage";
            SetPrimaryStats(5, 1, 1, 8);
            SetSecondaryStats();
            List<BaseWeapon.WType> allowedWeapons = new();
            List<BaseArmor.AType> allowedArmor = new();
            allowedWeapons.Add(BaseWeapon.WType.Staff);
            allowedWeapons.Add(BaseWeapon.WType.Wand);
            allowedArmor.Add(BaseArmor.AType.Cloth);
            AllowedArmorType = allowedArmor;
            AllowedWeaponType = allowedWeapons;
        }

        public override void LevelUp(int increase = 1)
        {
            if (increase < 1)
            {
                throw new ArgumentException();
            }
            GainLevel(1, 3, 1, 1, 5, increase);
        }
        public override void ReturnDamage()
        {
            CalcDamage(TotalInt);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Stats
    {
        // Primary stats
        
        public double Vitality { get; set; }
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

        public double TotalVit { get; set; }
        public double TotalStr { get; set; }
        public double TotalDex { get; set; }
        public double TotalInt { get; set; }

        public Dictionary<string, double> PrimaryStats = new();


        // Secondary stats
        public double Health { get; set; }
        public double ArmorRating { get; set; }
        public double ElementalResistance { get; set; }

        public Dictionary<string, double> SecondaryStats = new();
    }
}

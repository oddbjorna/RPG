﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Armor : BaseArmor
    {
        public AType ArmorType { get; set; }

        /// <summary>
        /// The Armor constructor takes an AType and an input string as parameters.
        /// If the input string is blank, a random level will be given to the piece of armor.
        /// The switch statement defines the armortype and gives the armor its stats.
        /// </summary>
        /// <param name="armorType"></param>
        /// <param name="inp"></param>
        public Armor(AType armorType, string inp = "")
        {
            ArmorType = armorType;
            int rInt = 1;
            if (inp == "")
            {
                Random random = new();
                rInt = random.Next(1, 50);
            }
            else
            {
                try
                {
                    rInt = int.Parse(inp);
                }
                catch
                {
                    Console.WriteLine($"Items can't have '{inp}' as level. Giving you armor with level 1.");
                }
            }

            switch (ArmorType)
            {
                case AType.Cloth:
                    Name = "Cloth Armor";
                    Vitality = 10 * rInt / 2;
                    Intelligence = 10 * rInt / 2;
                    Dexterity = 1 * rInt / 2;
                    Strength = 1 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case AType.Leather:
                    Name = "Leather Armor";
                    Vitality = 10 * rInt / 2;
                    Dexterity = 10 * rInt / 2;
                    Intelligence = 1 * rInt / 2;
                    Strength = 1 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case AType.Mail:
                    Name = "Mail Armor";
                    Vitality = 10 * rInt / 2;
                    Dexterity = 10 * rInt / 2;
                    Strength = 10 * rInt / 2;
                    Intelligence = 1 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case AType.Plate:
                    Name = "Plate Armor";
                    Vitality = 10 * rInt / 2;
                    Strength = 10 * rInt / 2;
                    Dexterity = 1 * rInt / 2;
                    Intelligence = 1 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
            }
        }
    }
}

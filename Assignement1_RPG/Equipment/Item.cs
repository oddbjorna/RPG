﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Item : Stats
    {
        public string Name { get; set; }
        public int LevelToEquip { get; set; }
        public double Damage { get; set; }
        public double AttacksPerSecond { get; set; }
        public double DPS { get => Damage * AttacksPerSecond; }

        // Available slots
        public enum Slot
        {
            Hand,
            Head,
            Chest,
            Legs,
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class BaseWeapon : Item
    {
        // WeaponTypes
        public enum WType
        {
            Axe,
            Bow,
            Daggers,
            Hammer,
            Staff,
            Sword,
            Wand
        }

    }
}

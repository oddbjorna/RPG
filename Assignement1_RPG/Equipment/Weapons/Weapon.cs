﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignement1_RPG
{
    public class Weapon : BaseWeapon
    {
        public WType WeaponType { get; set; }

        /// <summary>
        /// The Weapon constructor takes a WType and an input string as parameters.
        /// If the input string is blank, a random level will be given to the weapon.
        /// The switch statement defines the weapontype and gives the weapon its stats.
        /// </summary>
        /// <param name="type"></param>
        /// <param name="inp"></param>
        public Weapon(WType type, string inp = "")
        {
            WeaponType = type;
            int rInt = 1;
            if (inp == "")
            {
                Random random = new();
                rInt = random.Next(1, 50);
            }
            else
            {
                try
                {
                    rInt = int.Parse(inp);
                }
                catch
                {
                    Console.WriteLine($"Items can't have '{inp}' as level.");
                }
            }

            switch (WeaponType)
            {
                case WType.Axe:
                    Name = "Axe";
                    Damage = 12 * rInt / 2;
                    AttacksPerSecond = 2;
                    Strength = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Bow:
                    Name = "Bow";
                    Damage = 9 * rInt / 2;
                    AttacksPerSecond = 2.3;
                    Dexterity = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Daggers:
                    Name = "Daggers";
                    Damage = 8 * rInt / 2;
                    AttacksPerSecond = 3.2;
                    Dexterity = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Hammer:
                    Name = "Hammer";
                    Damage = 30 * rInt / 2;
                    AttacksPerSecond = 0.5;
                    Strength = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Staff:
                    Name = "Staff";
                    Damage = 18 * rInt / 2;
                    AttacksPerSecond = 1;
                    Intelligence = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Sword:
                    Name = "Sword";
                    Damage = 15 * rInt / 2;
                    AttacksPerSecond = 1.5;
                    Strength = 10 * rInt / 2;
                    Dexterity = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
                case WType.Wand:
                    Name = "Wand";
                    Damage = 8 * rInt / 2;
                    AttacksPerSecond = 3;
                    Intelligence = 10 * rInt / 2;
                    LevelToEquip = rInt;
                    break;
            }
        }
    }
}

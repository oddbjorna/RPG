﻿using System;
using System.Text;

namespace Assignement1_RPG
{
    class Program
    {
        /// <summary>
        /// The PrintStats method is logging the current characters stats to the console.
        /// It takes the 'hero' BaseClass parameter which is reffering to the current character.
        /// </summary>
        /// <param name="hero"></param>
        public static void PrintStats(BaseClass hero)
        {
            StringBuilder sb = new($"*    Your level is: {hero.Level} \n");

            sb.Append("*    Primary stats:\n");
            sb.Append($"*    Vitality: {hero.TotalVit}\n");
            sb.Append($"*    Strength: {hero.TotalStr}\n");
            sb.Append($"*    Dexterity: {hero.TotalDex}\n");
            sb.Append($"*    Intelligence: {hero.TotalInt}\n");

            sb.Append("*    Secondary stats:\n");
            sb.Append($"*    Health: {hero.Health}\n");
            sb.Append($"*    Armor Rating: {hero.ArmorRating}\n");
            sb.Append($"*    Elemental Resistance: {hero.ElementalResistance}\n");

            sb.Append($"*    DPS: {hero.TotalDPS}");

            Console.WriteLine(sb);
        }

        /// <summary>
        /// The CharacterChooser method is the method run by the Main method. It instanciates a new character with a given class.
        /// </summary>
        public static void CharacterChooser()
        {
            Console.WriteLine("*    Choose a class [ Mage, Ranger, Rogue, Warrior ]:");
            string input = Console.ReadLine();
            BaseClass hero;

            switch (input.ToLower())
            {
                case "mage":
                    hero = new Mage();
                    Run(hero);
                    break;
                case "ranger":
                    hero = new Ranger();
                    Run(hero);
                    break;
                case "rogue":
                    hero = new Rogue();
                    Run(hero);
                    break;
                case "warrior":
                    hero = new Warrior();
                    Run(hero);
                    break;
                default:
                    Console.WriteLine("*    Not a class");
                    CharacterChooser();
                    break;
            }
        }

        /// <summary>
        /// The PrintWepStats method is simply printing the stats of the currently equipped weapon.
        /// It takes the 'hero' parameter to access the item in the 'Hand' slot of the chracters equipment.
        /// </summary>
        /// <param name="hero"></param>
        public static void PrintWepStats(BaseClass hero)
        {
            try
            {
                Item weapon = hero.equipment[Item.Slot.Hand];
                StringBuilder wepStats = new($"*    Currently equipped weapon: {hero.equipment[Item.Slot.Hand].Name}\n");
                wepStats.Append($"*    Stats:\n");
                wepStats.Append($"*    Level: {weapon.LevelToEquip}");
                wepStats.Append($"    Vitality: {weapon.Vitality}");
                wepStats.Append($"    Strength: {weapon.Strength}");
                wepStats.Append($"    Dexterity: {weapon.Dexterity}");
                wepStats.Append($"    Intelligence: {weapon.Intelligence}");
                wepStats.Append($"    Damage: {weapon.Damage}");
                wepStats.Append($"    Attack Speed: {weapon.AttacksPerSecond}");
                wepStats.Append($"    DPS: {weapon.DPS}");
                Console.WriteLine(wepStats);
            }
            catch
            {
                Console.WriteLine("No weapon equipped.");
            }
        }

        /// <summary>
        /// The Run method is the 'GameLoop' method. You can input certain commands in the console to execute further methods.
        /// 'l' and 'lmax' will level up your character and calculate the apropriate damage stat. 'l' will level up your character by 1, while 'lmax' will level you instantly to 50.
        /// 'ew' will promt the user to equip a weapon. It creates a new weapon with the appropriate stats and level depending on the hero's class and the input of the user.
        /// 'ea' will promt the user to equip a piece of armor. It works fairly similar to 'ew' but you can chooose which slot to put the armor in.
        /// 'dps' simply return current hero's total DPS.
        /// 'wep' will print out the currently equipped weapons stats.
        /// 'eq' will print out the currently equipped items of the character and their level.
        /// 'stop' will end the game loop.
        /// </summary>
        /// <param name="hero"></param>
        public static void Run(BaseClass hero)
        {
            Console.WriteLine($"*    Welcome {hero.ClassName}!");

            PrintStats(hero);

            bool x = true;
            while (x)
            {
                string inp = Console.ReadLine();
                switch (inp)
                {
                    case "l":
                        hero.LevelUp();
                        hero.ReturnDamage();
                        PrintStats(hero);
                        break;
                    case "lmax":
                        hero.LevelUp(49);
                        hero.ReturnDamage();
                        PrintStats(hero);
                        break;
                    case "ew":
                        Console.WriteLine("*    What weapon will you equip? [   Wand, Staff, Sword, Daggers, Bow, Axe   ]\n*    (Leave blank for random weapon)");
                        string weaponInput = Console.ReadLine();
                        Weapon wep = EquipWeaponBase(weaponInput);
                        try
                        {
                            hero.EquipWeapon(wep);
                            hero.ReturnDamage();
                            PrintStats(hero);
                        }
                        catch (InvalidWeaponException ex)
                        {
                            Console.WriteLine($"InvalidWeaponException caught: {ex.Message}");
                        }
                        break;
                    case "ea":
                        Console.WriteLine("*    What armor will you equip? [   Cloth, Leather, Mail, Plate   ]");
                        string armorInput = Console.ReadLine();
                        Console.WriteLine("*    Where would you like to equip it? [   Chest, Legs, Head   ]");
                        string slotInp = Console.ReadLine();
                        Armor arm = EquipArmorBase(armorInput);
                        Item.Slot slot = ConvertStringToSlot(slotInp);
                        try
                        {
                            hero.EquipArmor(arm, slot);
                            hero.ReturnDamage();
                            PrintStats(hero);
                        }
                        catch (InvalidArmorException ex)
                        {
                            Console.WriteLine($"InvalidArmorException caught: {ex.Message}");
                        }
                        
                        break;
                    case "dps":
                        hero.ReturnDamage();
                        break;
                    case "wep":
                        PrintWepStats(hero);
                        break;
                    case "eq":
                        OpenEquipment(hero);
                        break;
                    case "stop":
                        x = false;
                        break;
                }
            }
        }

        /// <summary>
        /// EquipWeaponBase is a preprocessor for equiping a weapon.
        /// It checks an input from the user and makes sure that the input is a compatible weapon type.
        /// If the input is blank the user will be given a random weapon with a level of 1. It is not guarantied that the character can equip this weapon.
        /// If the input is matching an existing weapon type, the user will be asked to add the preffered level. If left blank the user will be given a random level (50 is max).
        /// The method then returns the newly created weapon to the next stage.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static Weapon EquipWeaponBase(string input = "")
        {
            if (input == "")
            {
                Array values = Enum.GetValues(typeof(BaseWeapon.WType));
                Random random = new();
                BaseWeapon.WType weapon = (BaseWeapon.WType)values.GetValue(random.Next(values.Length));
                Weapon equippedWeapon = new(weapon);

                return equippedWeapon;
            }
            else
            {
                try
                {
                    char.ToUpper(input[0]);
                    BaseWeapon.WType weapon = (BaseWeapon.WType)Enum.Parse(typeof(BaseWeapon.WType), input, true);
                    bool exists = Enum.IsDefined(typeof(BaseWeapon.WType), weapon);
                    if (!exists)
                    {
                        Console.WriteLine("*    That is not a weapon! Giving you a random weapon.");
                        return EquipWeaponBase();
                    }
                    else
                    {
                        Console.WriteLine("Enter a level. Leave blank for random");
                        string lvl = Console.ReadLine();
                        Weapon equippedWeapon = new(weapon, lvl);
                        return equippedWeapon;
                    }
                }
                catch
                {
                    Console.WriteLine("*    That is not a weapon! Giving you a random weapon.");
                    return EquipWeaponBase();
                }
            }
        }

        /// <summary>
        /// EquipArmorBase is working fairly similar to EquipWeaponBase.
        /// It returns a random armor type if the input is blank.
        /// If the input is not blank it checks if the armor type exists. If it does the user can add a level to it or get a random level.
        /// If the armor type does not exist the user is given a random armor type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static Armor EquipArmorBase(string type = "")
        {
            if (type == "")
            {
                Array values = Enum.GetValues(typeof(BaseArmor.AType));
                Random random = new();
                BaseArmor.AType armor = (BaseArmor.AType)values.GetValue(random.Next(values.Length));
                Armor equippedArmor = new(armor);

                return equippedArmor;
            }
            else
            {
                try
                {
                    char.ToUpper(type[0]);
                    BaseArmor.AType armor = (BaseArmor.AType)Enum.Parse(typeof(BaseArmor.AType), type, true);
                    bool exists = Enum.IsDefined(typeof(BaseArmor.AType), armor);
                    if (!exists)
                    {
                        Console.WriteLine("*    That is not an armor type! Giving you a random armor type.");
                        return EquipArmorBase();
                    }
                    else
                    {
                        Console.WriteLine("Enter a level. Leave blank for random");
                        string lvl = Console.ReadLine();
                        Armor equippedArmor = new(armor, lvl);
                        return equippedArmor;
                    }
                }
                catch
                {
                    Console.WriteLine("*    That is not an armor type! Giving you a random armor type.");
                    return EquipArmorBase();
                }
            }
        }

        /// <summary>
        /// ConvertStringToSlot is also quite similar to EquipWeaponBase and EquipArmorBase.
        /// It matches the input against the Item.Slot enumerator and returns the slot if the input is correct.
        /// If not the method will return a random slot. This method is used for the armor equip method where armor can go in multiple slots.
        /// However the method could return the 'Hand' slot, in which case armor can not go in. This issue is handled later in the process.
        /// </summary>
        /// <param name="inp"></param>
        /// <returns></returns>
        public static Item.Slot ConvertStringToSlot(string inp = "")
        {
            if (inp == "")
            {
                Array values = Enum.GetValues(typeof(Item.Slot));
                Random random = new();
                Item.Slot slot = (Item.Slot)values.GetValue(random.Next(values.Length));

                return slot;
            }
            else
            {
                try
                {
                    char.ToUpper(inp[0]);
                    Item.Slot slot = (Item.Slot)Enum.Parse(typeof(Item.Slot), inp, true);
                    bool exists = Enum.IsDefined(typeof(Item.Slot), slot);
                    if (!exists)
                    {
                        Console.WriteLine("*    That is not an equipable slot! Giving you a random slot.");
                        return ConvertStringToSlot();
                    }
                    else
                    {
                        return slot;
                    }
                }
                catch
                {
                    Console.WriteLine("*    That is not an equipable slot! Giving you a random slot.");
                    return ConvertStringToSlot();
                }
            }
            
        }

        /// <summary>
        /// The OpenEquipment is a simple method for printing the equipment to the console.
        /// The method is called by 'eq' in the 'Run' method.
        /// </summary>
        /// <param name="hero"></param>
        public static void OpenEquipment(BaseClass hero)
        {
            StringBuilder sb = new("*    Currently equipped items:\n*");
            foreach (Item.Slot slot in hero.equipment.Keys)
            {
                sb.Append($"    Equipped in {slot}: {hero.equipment[slot].Name}(lvl: {hero.equipment[slot].LevelToEquip})");
            }
            Console.WriteLine(sb);
        }

        public static void Main()
        {
            CharacterChooser();
        }
    }
}

﻿using Assignement1_RPG;
using System;
using System.Collections.Generic;
using Xunit;

namespace RPGTester
{
    public class ItemTest
    {
        [Fact]
        public void EquipWeapon_TryToEquipAHighLevelWeapon_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new();
            Weapon weapon = new(BaseWeapon.WType.Axe, "2");

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapon));
        }

        [Fact]
        public void EquipArmor_TryToEquipAHighLevelArmor_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new();
            Armor armor = new(BaseArmor.AType.Plate, "2");

            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor, Item.Slot.Chest));
        }

        [Fact]
        public void EquipWeapon_TryToEquipWrongTypeOfWeapon_ShouldThrowInvalidWeaponException()
        {
            Warrior warrior = new();
            Weapon weapon = new(BaseWeapon.WType.Bow, "1");

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(weapon));
        }

        [Fact]
        public void EquipArmor_TryToEquipWrongTypeOfArmor_ShouldThrowInvalidArmorException()
        {
            Warrior warrior = new();
            Armor armor = new(BaseArmor.AType.Cloth, "1");

            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(armor, Item.Slot.Chest));
        }

        [Fact]
        public void EquipWeapon_TryToEquipAValidWeapon_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new();
            Weapon weapon = new(BaseWeapon.WType.Axe, "1");

            string expected = "New Weapon Equipped!";

            string actual = warrior.EquipWeapon(weapon);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_TryToEquipAValidArmor_ShouldReturnSuccessMessage()
        {
            Warrior warrior = new();
            Armor armor = new(BaseArmor.AType.Plate, "1");

            string expected = "New Armor Equipped!";

            string actual = warrior.EquipArmor(armor, Item.Slot.Chest);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WarriorLevel1WithNoWeaponEquipped_ShouldReturnCorrectValue()
        {
            Warrior warrior = new();

            double expected = 1 * (1 + (5 / 100));

            double actual = warrior.TotalDPS;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WarriorLevel1WithAxeEquipped_ShouldReturnCorrectValue()
        {
            Warrior warrior = new();
            Weapon weapon = new(BaseWeapon.WType.Axe, "1");
            warrior.EquipWeapon(weapon);
            warrior.ReturnDamage();

            double expected = 12.0 / 2.0 * 2.0 * (1.0 + ((5.0 + 10.0 / 2.0) / 100.0));

            double actual = warrior.TotalDPS;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_WarriorLevel1WithAxeAndArmorEquipped_ShouldReturnCorrectValue()
        {
            Warrior warrior = new();
            Weapon weapon = new(BaseWeapon.WType.Axe, "1");
            Armor armor = new(BaseArmor.AType.Plate, "1");
            warrior.EquipWeapon(weapon);
            warrior.EquipArmor(armor, Item.Slot.Chest);
            warrior.ReturnDamage();

            double expected = 12.0 / 2.0 * 2.0 * (1.0 + ((5.0 + (10.0 / 2.0) + (10.0 / 2.0)) / 100.0));

            double actual = warrior.TotalDPS;

            Assert.Equal(expected, actual);
        }
    }
}
﻿using Assignement1_RPG;
using System;
using System.Collections.Generic;
using Xunit;

namespace RPGTester
{
    public class CharacterTest
    {
        [Fact]
        public void CreateCharacter_CreatingANewMageCharacter_ShouldReturnALevel1Mage()
        {
            Mage mage = new();
            int expected = 1;

            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelingUpACharacter_ShouldReturnCharacterLevelEquals2()
        {
            Ranger ranger = new();
            ranger.LevelUp();

            int expected = 2;

            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_LevelingUpACharacterIncorrectly_ShouldThrowArgumentException(int level)
        {
            Warrior warrior = new();

            Assert.Throws<ArgumentException>(() => warrior.LevelUp(level));
        }

        [Fact]
        public void CreateCharacter_UsingALevel1Mage_ShouldReturnProperLevel1Stats()
        {
            Mage mage = new();
            Dictionary<string, double> expected = new() { { "Vitality", 5 }, { "Strength", 1 }, { "Dexterity", 1 }, { "Intelligence", 8 } };

            Dictionary<string, double> actual = mage.PrimaryStats;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateCharacter_UsingALevel1Warrior_ShouldReturnProperLevel1Stats()
        {
            Warrior warrior = new();
            Dictionary<string, double> expected = new() { { "Vitality", 10 }, { "Strength", 5 }, { "Dexterity", 2 }, { "Intelligence", 1 } };

            Dictionary<string, double> actual = warrior.PrimaryStats;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateCharacter_UsingALevel1Ranger_ShouldReturnProperLevel1Stats()
        {
            Ranger ranger = new();
            Dictionary<string, double> expected = new() { { "Vitality", 8 }, { "Strength", 1 }, { "Dexterity", 7 }, { "Intelligence", 1 } };

            Dictionary<string, double> actual = ranger.PrimaryStats;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateCharacter_UsingALevel1Rogue_ShouldReturnProperLevel1Stats()
        {
            Rogue rogue = new();
            Dictionary<string, double> expected = new() { { "Vitality", 8 }, { "Strength", 2 }, { "Dexterity", 6 }, { "Intelligence", 1 } };

            Dictionary<string, double> actual = rogue.PrimaryStats;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_IncreaseMageStats_ShouldReturnIncreasedStatsWhenGainingALevel()
        {
            Mage mage = new();
            mage.LevelUp();

            Dictionary<string, double> expected = new() { { "Vitality", 8}, { "Strength", 2}, { "Dexterity", 2}, { "Intelligence", 13 } };

            Dictionary<string, double> actual = mage.PrimaryStats;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_IncreaseWarriorStats_ShouldReturnIncreasedStatsWhenGainingALevel()
        {
            Warrior warrior = new();
            warrior.LevelUp();

            Dictionary<string, double> expected = new() { { "Vitality", 15 }, { "Strength", 8 }, { "Dexterity", 4 }, { "Intelligence", 2 } };

            Dictionary<string, double> actual = warrior.PrimaryStats;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_IncreaseRangerStats_ShouldReturnIncreasedStatsWhenGainingALevel()
        {
            Ranger ranger = new();
            ranger.LevelUp();

            Dictionary<string, double> expected = new() { { "Vitality", 10 }, { "Strength", 2 }, { "Dexterity", 12 }, { "Intelligence", 2 } };

            Dictionary<string, double> actual = ranger.PrimaryStats;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void LevelUp_IncreaseRogueStats_ShouldReturnIncreasedStatsWhenGainingALevel()
        {
            Rogue rogue = new();
            rogue.LevelUp();

            Dictionary<string, double> expected = new() { { "Vitality", 11 }, { "Strength", 3 }, { "Dexterity", 10 }, { "Intelligence", 2 } };

            Dictionary<string, double> actual = rogue.PrimaryStats;

            Assert.Equal(expected, actual);

        }

        [Fact]
        public void SetSecondaryStats_IncreaseSecondaryStatsForWarrior_ShouldReturnCorrectSecondaryStats()
        {
            Warrior warrior = new();
            warrior.LevelUp();

            Dictionary<string, double> expected = new() { { "Health", 150 }, { "ArmorRating", 12 }, { "ElementalResistance", 2 } };

            Dictionary<string, double> actual = warrior.SecondaryStats;

            Assert.Equal(expected, actual);

        }
    }

}
